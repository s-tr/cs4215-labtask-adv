{-# LANGUAGE ExplicitForAll, Rank2Types #-}

{-
	TA's email:
	mahmudulfaisal@gmail.com
-}

{-
	Forward simulation is easy, however backward computation is much harder to
	do. Here we only implement the forward case and a simple simulation-based
	model.
-}

module Probability where

import Data.List
import System.Random
import Data.Maybe
import Control.Monad

data RandVal t = RandFunc ([Double] -> Maybe (t,[Double]))
               
instance Monad RandVal where
	{-
		Introduces a constant value into the RandVal monad.
	-}
    return a = RandFunc (\ds -> Just (a,ds))
    {-
		The monadic bind function.
    -}
    (RandFunc f0) >>= f = RandFunc $ k
                   where
                     k ds = case f0 ds of
                       Nothing -> Nothing
                       Just (a,ds') ->
                         let RandFunc f1 = f a
                         in  f1 ds'
    {-
		Allows a random computation to fail gracefully and be retried with
		different randomness.
    -}
    fail _   = RandFunc (\_ -> Nothing)

instance Functor RandVal where 
    {-
		Just a regular Functor definition based off Monad
		
		(Monad m) => Functor m when
    -}
    fmap f r = r >>= (return . f)

runRand :: RandVal t -> IO (Maybe t)
runRand (RandFunc f) = do
                           gen <- newStdGen
                           case f$randoms gen of
                               Nothing -> return Nothing
                               Just (a,_) -> return (Just a)

{-
	runRands runs n trials of e, and filters out fail results before returning
	the results.
-}

runRands :: Int -> RandVal t -> IO [t]
runRands n e = do
                  res <- sequence $ map runRand $ replicate n $ e
                  return $ catMaybes res

{-
	Retries more trials until n successes.
-}
runRandsUntil :: Int -> RandVal t -> IO [t]
runRandsUntil n e = do
                        r1 <- runRands n e
                        let len = length r1
                        if len<n then fmap (r1++) (runRandsUntil (n-len) e) 
                        else return r1

{-
	A "random" variable that always returns the same result.
-}
constant :: t -> RandVal t
constant = return

{-
	A random variable that returns (true) with probability p and false otherwise
-}
flipCoin :: Double -> RandVal Bool
flipCoin p = RandFunc (\(d:ds) -> Just (d<p,ds))

{-
	For input [(p1,v1),(p2,v2),...], returns v1 with probability p1, v2 with
	probability p2, etc.
-}
select :: [(Double, t)] -> RandVal t
select = undefined

{-
	Creates a uniform distribution between k1 and k2.
-}
uniform :: Double -> Double -> RandVal Double
uniform k1 k2 = RandFunc (\(d:ds) -> Just (k1+d*(k2-k1),ds))

{-
	Creates a discrete uniform distribution between k1 (incl) and k2 (excl).
-}
uniformD :: (Integral a) => a -> a -> RandVal a
uniformD k1 k2 = RandFunc (\(d:ds) -> Just (k1+floor(d*(fromIntegral$k2-k1)),ds))

{-
	Creates an exponential distribution with mean l.
-}
expDist :: Double -> RandVal Double
expDist l = RandFunc (\(d:ds) -> Just (-l*(log d),ds))

{-
	Creates an discrete exponential (i.e. geometric) distribution with mean l.
-}
expDistD :: (Integral a) => a -> RandVal a
expDistD l = RandFunc (\(d:ds) -> Just (ceiling ((log d)/(log (1-1/(fromIntegral l)))),ds))

testFunc :: RandVal String
testFunc = do
               b1 <- flipCoin 0.7
               return$if b1 then "Got Heads!" else "Got Tails!"	

{-
	Imposes a condition on an expression - it will produce a failure if it does
	not satisfy the specified predicate.
-}
condition :: (t -> Bool) -> (RandVal t) -> (RandVal t)
condition p e = do
                    a <- e
                    if p a then return a else fail ""

{-
	Returns the probability of a RandVal satisfying a predicate p in n trials.
-}
probability :: (t -> Bool) -> Int -> RandVal t -> IO Double
probability p n e = do
                        res <- runRandsUntil n e
                        let len = length res
                        let cnt = length $ filter p res
                        return $ (fromIntegral cnt) / (fromIntegral len)

{-
	Examples here~
-}

{-
	Six-sided die.
-}
dice6 :: RandVal Int
dice6 = uniformD 1 7

{-
	Rolling some dice together...
-}
threeDice :: RandVal Int
threeDice = fmap sum $ sequence [dice6,dice6,dice6]

{-
	What is the probability of three dice returning a specific number?
-}
prob :: Int -> Int -> IO Double
prob n k = probability (==k) n threeDice

{-
	What if we now restrict the possible rolls, so that we have to discard the
	roll if we roll above 11?
-}
lowThreeDice = condition (<=11) threeDice

{-
	Now...?
	What is the probability of three dice returning a specific number?
-}
prob' :: Int -> Int -> IO Double
prob' n k = probability (==k) n lowThreeDice
