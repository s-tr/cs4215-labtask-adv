{-# LANGUAGE ExplicitForAll, Rank2Types #-}

{-
	TA's email:
	mahmudulfaisal@gmail.com
-}

{-
	Forward simulation is easy, however backward computation is much harder to
	do. Here we only implement a simple simulation-based backward model.
-}

module ProbabilityTracked where

import Data.List
import System.Random
import Data.Maybe
import Control.Monad
import qualified Control.Monad.Fail as F

{-
	The function argument takes a stream of random values, eats some of them to
	produce either a result or a failure. When a result is produced, the return
	value is Just (result, remaining randoms) and failure produces Nothing.
-}
data RandVal t = RandFunc ([Double] -> Maybe (t,[Double]))
               
instance Monad RandVal where
	{-
		Introduces a value into the RandVal monad.
	-}
    return a = RandFunc (\ds -> Just (a,ds))
    {-
		The monadic bind function.
    -}
    (RandFunc f0) >>= f = RandFunc $ k
        where
            k ds = case f0 ds of
                Nothing -> Nothing
                Just (a,ds') ->
                    let RandFunc f1 = f a
                    in  f1 ds'
    fail = F.fail

{-
	Using MonadFail cause fail is going to be removed from Monad in the future
-}
instance F.MonadFail RandVal where
    fail _   = RandFunc (\_ -> Nothing)

instance Functor RandVal where 
    {-
		Just a regular Functor definition based off Monad
		
		(Monad m) => Functor m when
    -}
    fmap f r = r >>= (return . f)

{-
	A failure value.
-}
failure :: RandVal a
failure = fail ""

{-
	Imposes a condition on an expression - it will produce a failure if it does
	not satisfy the specified predicate.
-}
condition :: (t -> Bool) -> (RandVal t) -> (RandVal t)
condition p e = do
    a <- e
    if p a then return a else failure

{-
	A "random" variable that always returns the same result.
-}
constant :: t -> RandVal t
constant = return

{-
	A random variable that returns True with probability p and false otherwise
-}
flipCoin :: Double -> RandVal Bool
flipCoin p = RandFunc (\(d:ds) -> Just (d<p,ds))

{-
	For input [(p1,v1),(p2,v2),...], returns v1 with probability p1, v2 with
	probability p2, etc.
-}
select :: [(Double, t)] -> RandVal t
select xs = RandFunc $ selector
    where
        selector (d:ds) = case selector_aux d xs of
		    Nothing -> Nothing
		    Just a  -> Just (a,ds)
        selector_aux _ [] = Nothing
        selector_aux d ((p,x):xs) = if d<=p then Just x else selector_aux (d-p) xs

{-
	Creates a uniform distribution between k1 and k2.
-}
uniform :: Double -> Double -> RandVal Double
uniform k1 k2 = RandFunc (\(d:ds) -> Just (k1+d*(k2-k1),ds))

{-
	Creates a discrete uniform distribution between k1 (incl) and k2 (excl).
-}
uniformD :: (Integral a) => a -> a -> RandVal a
uniformD k1 k2 = RandFunc (\(d:ds) -> Just (k1+floor(d*(fromIntegral$k2-k1)),ds))

{-
	A random integer between 1 and x (inclusive)
-}
roll :: (Integral a) => a -> RandVal a
roll x = uniformD 1 (x+1)

{-
	Creates an exponential distribution with mean l.
-}
expDist :: Double -> RandVal Double
expDist l = RandFunc (\(d:ds) -> Just (-l*(log d),ds))

{-
	Creates an discrete exponential (i.e. geometric) distribution with mean l.
-}
expDistD :: (Integral a) => a -> RandVal a
expDistD l = RandFunc (\(d:ds) -> Just (ceiling ((log d)/(log (1-1/(fromIntegral l)))),ds))

selectRandom :: [a] -> RandVal a
selectRandom [] = fail ""
selectRandom xs = do
    let len = length xs
    index <- uniformD 0 len
    return (xs !! index)

runRand :: RandVal t -> IO (Maybe t)
runRand (RandFunc f) = do
    gen <- newStdGen
    case f$randoms gen of
        Nothing -> return Nothing
        Just (a,_) -> return (Just a)

{-
	runRands runs n trials of e, and filters out fail results before returning
	the results.
-}
runRands :: Int -> RandVal t -> IO [t]
runRands n e = do
    res <- sequence $ map runRand $ replicate n $ e
    return $ catMaybes res

{-
	Retries more trials until n successes.
-}
runRandsUntil :: Int -> RandVal t -> IO [t]
runRandsUntil n e = do
    r1 <- runRands n e
    let len = length r1
    if len<n then fmap (r1++) (runRandsUntil (n-len) e) 
    else return r1

testRand :: (RandVal a) -> (RandVal a -> RandVal b) -> IO (Maybe (a,b))
testRand (RandFunc f0) expr = do
    gen <- newStdGen
    let rs   = randoms gen
    let res1 = f0 rs
    case res1 of
        Nothing -> return Nothing
        Just (x,rs')  -> 
            let (RandFunc f1) = expr (constant x)
            in  case (f1 rs') of
                    Nothing -> return Nothing
                    Just (y,_) -> return $ Just (x,y)
{-
	For a random variable e1 and another variable expr which depends on e1, finds
	(a,b) pairs such that:
	    a <- e1
	    b <- expr(a)
	    b did not result in failure
-}
testRands :: (RandVal a) -> (RandVal a -> RandVal b) -> Int -> IO [(a,b)]
testRands e1 expr n = fmap catMaybes $ sequence $ replicate n (testRand e1 expr)

{-
	Keeps running testRands until we get n non-failure results.
-}
testRandsUntil :: (RandVal a) -> (RandVal a -> RandVal b) -> Int -> IO [(a,b)]
testRandsUntil e1 expr n = do
    res <- testRands e1 expr n
    let len = length res
    if   len < n then fmap (++res) (testRandsUntil e1 expr (n-len))
    else return res

{-
	Returns the probability of a RandVal satisfying a predicate p in n trials.
-}
probability :: (t -> Bool) -> RandVal t -> Int -> IO Double
probability p e n = do
    res <- runRandsUntil n e
    let len = length res
    let cnt = length $ filter p res
    return $ (fromIntegral cnt) / (fromIntegral len)

distribution :: (Ord t) => Int -> RandVal t -> IO [(t,Int)]
distribution n e = do
    res <- runRandsUntil n e
    return $ map (\xs@(x:_) -> (x,length xs)) $ group $ sort res

{-
	Given:
		e1, the base expression which we want to analyse,
		expr(e1), an expression which depends on e1,
		p2, a predicate to be placed on expr(e1),
		p1, a test to be made on the values of e1,
		n, the number of trials to run,
	
	Returns P(p1(e1) | p2(e2)) with n trials.
-}
condProb :: RandVal t -> (RandVal t -> RandVal s) -> (s -> Bool) -> (t -> Bool) -> Int -> IO Double
condProb e1 expr p2 p1 n = do
    let expr' = condition p2 . expr
    res <- fmap (map fst) $ (testRandsUntil e1 expr' n)
    let len = length res
    let cnt = length $ filter p1 res
    return ((fromIntegral cnt)/(fromIntegral len))

{-
	Examples
-}

dice1 = roll 6

{-
	Sums two dice. We want to track the probability distribution of the first
	dice given a constraint (p) on the result.
-}
twoDice :: (Integer -> Bool) -> RandVal Integer -> RandVal Integer
twoDice p d1 = do
    let d2 = roll 6
    roll1 <- d1
    roll2 <- d2
    if   p (roll1+roll2) then return (roll1+roll2)
    else fail ""

{-
	Let's simulate the Monty Hall problem!
-}
montyHall :: Bool -> RandVal Bool
montyHall switch = do
    doorWithCar <- roll 3
    selectedDoor <- roll 3
    hostDoor <- selectRandom ([1,2,3] \\ [selectedDoor,doorWithCar])
    let otherDoor = head ([1,2,3] \\ [selectedDoor,hostDoor])
    let finalSelection = if switch then otherDoor else selectedDoor
    return (finalSelection == doorWithCar)

{-
	Testing patients for a disease
-}
{-
	Tests a patient with disease status (status) and gives a probabilistic
	result depending on whether they actually have the disease.
-}
citizen = flipCoin 0.01

testPatient :: Bool -> RandVal Bool
testPatient status = if status then constant True else flipCoin 0.05

diseaseTest1 :: RandVal Bool -> RandVal ()
diseaseTest1 patient = do
    result <- patient >>= testPatient
    if result then return () else failure

diseaseTest2 :: RandVal Bool -> RandVal ()
diseaseTest2 patient = do
    result1 <- patient >>= testPatient
    if   not result1 then failure
    else do
            result2 <- patient >>= testPatient
            if result2 then return () else failure

diseaseTest2' :: RandVal Bool -> RandVal ()
diseaseTest2' patient = do
    result1 <- patient >>= testPatient
    result2 <- patient >>= testPatient
    if result1 && result2 then return () else failure

diseaseTest3 :: RandVal Bool -> RandVal ()
diseaseTest3 patient = do
    result1 <- patient >>= testPatient
    result2 <- patient >>= testPatient
    result3 <- patient >>= testPatient
    if result1 && result2 && result3 then return () else failure
